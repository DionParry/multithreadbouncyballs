
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Title: Lab 9 - Multi Threaded Graphics
 * Date: 04/12/2016
 * @author Dion
 */
public class BallFrame extends JFrame implements ActionListener, ChangeListener {
    
    /**
     * initialise components
     */
    private BallComponent component;
    public static final int DEFAULT_WIDTH = 450;
    public static final int DEFAULT_HEIGHT = 350;
    private JButton start;
    private JButton pause;
    private JButton stop;
    private JButton collision;
    private JSlider slider;
    private int sliderValue = 6;
    private static Random rand = new Random();

    /**
     * collisions boolean for method toggle
     */
    public static boolean collisions = false;

    private static final Color[] colors = {Color.BLACK, Color.BLUE,
        Color.CYAN, Color.DARK_GRAY, Color.GRAY, Color.GREEN,
        Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE, Color.PINK,
        Color.RED, Color.WHITE, Color.YELLOW};

    /**
     * creates a frame of buttons and balls
     */
    public BallFrame() {

        setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
        setTitle("BounceThread");

        component = new BallComponent();
        add(component, BorderLayout.CENTER);
        JPanel buttonPanel = new JPanel();

        start = new JButton("Start");
        start.addActionListener(this);
        buttonPanel.add(start);

        pause = new JButton("Pause");
        pause.addActionListener(this);
        buttonPanel.add(pause);

        stop = new JButton("Stop");
        stop.addActionListener(this);
        buttonPanel.add(stop);

        collision = new JButton("Collision - ON");
        collision.addActionListener(this);
        buttonPanel.add(collision);

        slider = new JSlider(JSlider.HORIZONTAL, 1, 5, 1);
        slider.setMajorTickSpacing(1);
        slider.setPaintLabels(true);
        slider.setPaintTicks(true);
        slider.addChangeListener(this);

        buttonPanel.add(slider);
        add(buttonPanel, BorderLayout.SOUTH);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(start)) {
            System.out.println("Start");
            addBall();
        }

        if (e.getSource().equals(pause)) {
            paused();
        }

        if (e.getSource().equals(stop)) {
            System.out.println("Closing.....");
            stop();
        }

        if (e.getSource().equals(collision)) {
            collide();
        }
    }

    /**
     * pause method
     */
    public void paused() {
        if (BallRunnable.running == false) {
            BallRunnable.running = true;
            pause.setText("UnPause");
        } else {
            BallRunnable.running = false;
            pause.setText("Pause");
        }
    }

    public void stateChanged(ChangeEvent e) {
        if (e.getSource().equals(slider)) {
            sliderValue = slider.getValue();
            System.out.println(sliderValue);
        }
    }

    /**
     * adds a new ball gives it a thread
     */
    public void addBall() {
        Ball b = new Ball(DEFAULT_HEIGHT, DEFAULT_WIDTH, newColor()); //add the colour here
        component.add(b);
        Runnable r = new BallRunnable(b, component, this);
        component.repaint();
        Thread t = new Thread(r);
        t.start();
        System.out.println(t);
    }

    private Color newColor() {
        return colors[rand.nextInt(colors.length)];
    }

    /**
     *
     * @return speed
     */
    public int newSpeed() {
        int speed = sliderValue;
        if (speed == 5) {
            speed = 1;
        } else if (speed == 4) {
            speed = 2;
        } else if (speed == 3) {
            speed = 3;
        } else if (speed == 2) {
            speed = 4;
        } else if (speed == 1) {
            speed = 5;
        }
        return speed; 
    }

    /**
     * stops the active threads after 5 seconds
     */
    public void stop() {
        try {
            Thread.currentThread().sleep(5000);
        } catch (InterruptedException exception) {
            // ignore the exception and continue
            System.out.println("have an exception");
        }
        component.clean();
    }
    
    /**
     * toggles the collision method
     */
    public void collide() {
      if (collisions == false) {
            collisions = true;
            collision.setText("Collision - OFF");
        } else {
            collisions = false;
            collision.setText("Collision - ON");
        }
    }
}
