import java.awt.Color;
import static java.awt.Color.BLUE;
import static java.awt.Color.GREEN;
import static java.awt.Color.RED;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JPanel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Title: Lab 9 - Multi Threaded Graphics
 * Date: 04/12/2016
 * @author Dion
 */
public class BallComponent extends JPanel {

    private ArrayList<Ball> balls = new ArrayList<Ball>();

    /**
     * * Add a ball to the component. * @param b the ball to add
     * @param a
     */
    public void add(Ball a) {
        balls.add(a);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g); // erase background
        Graphics2D g2 = (Graphics2D) g;
        
        for (int i = 0; i < balls.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (balls.get(i).collide(balls.get(j))) {
                    balls.get(i).collision();
                    balls.get(j).collision();
                //    balls.remove(balls.get(i));
               // g2.fill(balls.get(i).getNewShape());
                }
            }
            g2.setColor(balls.get(i).getColor());
            g2.fill(balls.get(i).getShape());
        }
    }

    /**
     * remove method for ball collisions
     * @param a
     */
    public void remove (Ball a) {
        balls.remove(a);
    }
    
    /**
     * clears all running threads from frame
     */
    public void clean() {
        balls.clear();
    }
}
