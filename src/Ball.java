import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.Random;
import static javafx.scene.paint.Color.color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Title: Lab 9 - Multi Threaded Graphics
 * Date: 04/12/2016
 * @author Dion
 */
public class Ball {

    private static final int XSIZE = 15;
    private static final int YSIZE = 15;
    private double x = 0;
    private double y = 0;
    private double dx = 1;
    private double dy = 1;
    private Color color;
    
    /**
     *
     * @return x
     */
    public double getX() {
        return x;
    }

    /**
     *
     * @return y
     */
    public double getY() {
        return y;
    }

    /**
     *
     * @return colour
     */
    public Color getColor() {
        return color;
    }

    /**
     * * Moves the ball to the next position, reversing direction if it hits
     * one of the edges
     * @param bounds
     */
    public void move(Rectangle2D bounds) {
        if (BallRunnable.running == false) {
            x += dx;
            y += dy;
            if (x < bounds.getMinX()) {
                x = bounds.getMinX();
                dx = -dx;
            }
            if (x + XSIZE >= bounds.getMaxX()) {
                x = bounds.getMaxX() - XSIZE;
                dx = -dx;
            }

            if (y < bounds.getMinY()) {
                y = bounds.getMinY();
                dy = -dy;
            }

            if (y + YSIZE >= bounds.getMaxY()) {
                y = bounds.getMaxY() - YSIZE;
                dy = -dy;
            }
        }
    }

    //Gets the shape of the ball at its current position.

    /**
     *
     * @return the shape of the ball
     */
    public Ellipse2D getShape() {
        return new Ellipse2D.Double(x, y, XSIZE, YSIZE);
    }
    
    /**
     *
     * @return the ball new shape
     */
    public Ellipse2D getNewShape() {
        if (BallFrame.collisions == false) {
            dx = -dx;
            dy = -dy;
        }
        return new Ellipse2D.Double(x, y, 5, 5);
    }

    /**
     *
     * @param other
     * @return
     */
    public boolean collide(Ball other) {
        if (((x - other.getX()) * (x - other.getX()) + 
                (y - other.getY()) * (y - other.getY())) < 100) {
            return true;
        } else {
            return false;
        }
    }

    Ball(int defaultWidth, int defaultHeight, Color color) {
        Random random = new Random();
        this.color = color;
        int border = random.nextInt(4);
        switch (border) {
            case 0:
                x = 0;
                y = random.nextInt(defaultHeight);
                break;
            case 1:
                y = 0;
                x = random.nextInt(defaultWidth);
                break;
            case 2:
                x = defaultWidth;
                y = random.nextInt(defaultHeight);
                break;
            case 3:
                y = defaultHeight;
                x = random.nextInt(defaultWidth);
                break;
            default:
                System.out.println("Error: random border was not found");
        }

    }

    /**
     * collision detection
     */
    public void collision() {
        if (BallFrame.collisions == false) {
            dx = -dx;
            dy = -dy;
        }
    }
}
