
import java.awt.Color;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Title: Lab 9 - Multi Threaded Graphics
 * Date: 04/12/2016
 * @author Dion
 */
public class BallRunnable implements Runnable {

    private Ball ball;
    private BallComponent component;
    private BallFrame frame;
    public static final int STEPS = 1000000000;
    private int speed;

    /**
     * collisions boolean for method toggle
     */
    public static boolean running = false;

    /**
     *
     * @param aBall
     * @param aComponent
     * @param aFrame
     */
    public BallRunnable(Ball aBall, BallComponent aComponent, BallFrame aFrame) {
        frame = aFrame;
        ball = aBall;
        component = aComponent;
    }
    
    @Override
    public void run() {
        try {
            while (true) {
                speed = frame.newSpeed();
                ball.move(component.getBounds());
                component.repaint();
                Thread.sleep(speed);
            }
        } catch (InterruptedException e) {
            System.out.println("fail....");
        }
    }
}
